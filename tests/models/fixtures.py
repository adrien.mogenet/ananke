import numpy as np
import pytest

from ananke.graphs import DAG
from ananke.models import discrete


@pytest.fixture
def simple_graph():
    graph = DAG(
        di_edges=[
            ("A", "Y"),
            ("U", "A"),
            ("U", "Y"),
            ("S", "A"),
            ("S", "Y"),
        ],
        vertices={"A": 2, "Y": 3, "U": 2, "S": 4},
    )
    contexts = [set(), {"A"}, {"Y"}, {"A", "Y"}]
    cards = {"A": 2, "Y": 3, "S": len(contexts), "U": 2}
    return graph, contexts, cards
