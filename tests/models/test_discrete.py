from collections import ChainMap

import numpy as np
import pytest
from pgmpy.factors.discrete import TabularCPD
from pgmpy.inference import VariableElimination
from pgmpy.models import BayesianNetwork

from ananke.graphs import ADMG, DAG
from ananke.identification import OneLineID
from ananke.models import discrete


class TestEffectEstimationID:
    def test_estimate_effect_from_distribution(self):
        di_edges = [("A", "Y"), ("C", "A"), ("C", "Y")]
        graph = ADMG(["A", "C", "Y"], di_edges=di_edges)
        net = BayesianNetwork(di_edges)
        net = net.get_random_cpds(n_states=2)
        treatment_dict = {"A": 1}
        outcome_dict = {"Y": 1}

        int_net = discrete.intervene(net, treatment_dict)
        int_inference = VariableElimination(int_net)
        truth = int_inference.query(["Y"]).get_value(**outcome_dict)

        oid = OneLineID(graph, list(treatment_dict), list(outcome_dict))
        effect = discrete.estimate_effect_from_discrete_dist(
            oid, net, treatment_dict, outcome_dict
        )
        #        effect = discrete.estimate_effect_from_discrete_dist(
        #            oid, net, treatment_dict, outcome_dict
        #        )
        assert truth == pytest.approx(effect)

    def test_estimate_effect_front_door(self):
        di_edges = [("A", "M"), ("M", "Y")]
        bi_edges = [("A", "Y")]
        graph = ADMG(
            vertices={"A": 2, "M": 2, "Y": 2},
            di_edges=di_edges,
            bi_edges=bi_edges,
        )
        dag = graph.canonical_dag(cardinality=2)
        cpds = discrete.generate_random_cpds(graph=dag, dir_conc=10)
        net = discrete.generate_bayesian_network(graph=dag, cpds=cpds)
        treatment_dict = {"A": 1}

        outcome_dict = {"Y": 1}

        int_net = discrete.intervene(net, treatment_dict)
        int_inference = VariableElimination(int_net)
        truth = int_inference.query(["Y"]).get_value(**outcome_dict)
        oid = OneLineID(graph, list(treatment_dict), list(outcome_dict))
        effect = discrete.estimate_effect_from_discrete_dist(
            oid, net, treatment_dict, outcome_dict
        )

        # effect = discrete.estimate_effect_from_discrete_dist(
        #    oid, net, treatment_dict, outcome_dict
        # )
        assert truth == pytest.approx(effect)


class TestDiscreteGraphOperations:
    def test_correct_cpds_generated(self, simple_graph):
        graph, contexts, cards = simple_graph

        result = discrete.generate_random_cpds(graph=graph, dir_conc=10)

    def test_interventions(self, simple_graph):
        graph, contexts, cards = simple_graph
        cpds = discrete.generate_random_cpds(graph=graph, dir_conc=10)

        net = discrete.generate_bayesian_network(graph, cpds)
        treatment_dict = {"A": 1}

        intervened_net = discrete.intervene(net, treatment_dict)

        assert (
            intervened_net.get_state_probability(
                {"A": 0, "Y": 0, "U": 0, "S": 0}
            )
            == 0
        )
